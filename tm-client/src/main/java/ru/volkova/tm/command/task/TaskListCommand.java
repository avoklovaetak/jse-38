package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Task;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[TASK LIST]");
        @Nullable final String sort = TerminalUtil.nextLine();
        List<Task> tasks = endpointLocator.getTaskEndpoint().findAllTasks(session);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println((index + ". " + task));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "task-show-list";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
