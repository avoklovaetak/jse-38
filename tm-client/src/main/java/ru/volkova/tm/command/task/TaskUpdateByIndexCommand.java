package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.endpoint.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update task by index";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = endpointLocator.getTaskEndpoint()
                .findTaskByIndex(session, index);
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint()
                .updateTaskByIndex(session, index, name, description);
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
