package ru.volkova.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.endpoint.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected EndpointLocator endpointLocator;

    @Nullable
    protected Bootstrap bootstrap;
    
    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

    public void setEndpointLocator(@NotNull EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    public void setBootstrap(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

}
