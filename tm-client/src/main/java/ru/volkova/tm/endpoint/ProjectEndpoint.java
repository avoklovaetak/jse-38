package ru.volkova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-10-11T22:23:20.410+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.volkova.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/changeProjectOneStatusByIdRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/changeProjectOneStatusByIdResponse")
    @RequestWrapper(localName = "changeProjectOneStatusById", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ChangeProjectOneStatusById")
    @ResponseWrapper(localName = "changeProjectOneStatusByIdResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ChangeProjectOneStatusByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project changeProjectOneStatusById(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "status", targetNamespace = "")
        ru.volkova.tm.endpoint.Status status
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/startProjectByIdRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/startProjectByIdResponse")
    @RequestWrapper(localName = "startProjectById", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.StartProjectById")
    @ResponseWrapper(localName = "startProjectByIdResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.StartProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project startProjectById(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/removeProjectByIdRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/removeProjectByIdResponse")
    @RequestWrapper(localName = "removeProjectById", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.RemoveProjectById")
    @ResponseWrapper(localName = "removeProjectByIdResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.RemoveProjectByIdResponse")
    public void removeProjectById(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/changeProjectStatusByNameRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/changeProjectStatusByNameResponse")
    @RequestWrapper(localName = "changeProjectStatusByName", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ChangeProjectStatusByName")
    @ResponseWrapper(localName = "changeProjectStatusByNameResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ChangeProjectStatusByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project changeProjectStatusByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "status", targetNamespace = "")
        ru.volkova.tm.endpoint.Status status
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/finishProjectByIdRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/finishProjectByIdResponse")
    @RequestWrapper(localName = "finishProjectById", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FinishProjectById")
    @ResponseWrapper(localName = "finishProjectByIdResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FinishProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project finishProjectById(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/finishProjectByIndexRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/finishProjectByIndexResponse")
    @RequestWrapper(localName = "finishProjectByIndex", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FinishProjectByIndex")
    @ResponseWrapper(localName = "finishProjectByIndexResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FinishProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project finishProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/startProjectByNameRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/startProjectByNameResponse")
    @RequestWrapper(localName = "startProjectByName", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.StartProjectByName")
    @ResponseWrapper(localName = "startProjectByNameResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.StartProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project startProjectByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findAllProjectsRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findAllProjectsResponse")
    @RequestWrapper(localName = "findAllProjects", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindAllProjects")
    @ResponseWrapper(localName = "findAllProjectsResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindAllProjectsResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.volkova.tm.endpoint.Project> findAllProjects(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/clearProjectRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/clearProjectResponse")
    @RequestWrapper(localName = "clearProject", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ClearProject")
    @ResponseWrapper(localName = "clearProjectResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ClearProjectResponse")
    public void clearProject(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findProjectByIndexRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findProjectByIndexResponse")
    @RequestWrapper(localName = "findProjectByIndex", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindProjectByIndex")
    @ResponseWrapper(localName = "findProjectByIndexResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project findProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/startProjectByIndexRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/startProjectByIndexResponse")
    @RequestWrapper(localName = "startProjectByIndex", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.StartProjectByIndex")
    @ResponseWrapper(localName = "startProjectByIndexResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.StartProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project startProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/changeProjectStatusByIndexRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/changeProjectStatusByIndexResponse")
    @RequestWrapper(localName = "changeProjectStatusByIndex", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ChangeProjectStatusByIndex")
    @ResponseWrapper(localName = "changeProjectStatusByIndexResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.ChangeProjectStatusByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project changeProjectStatusByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "status", targetNamespace = "")
        ru.volkova.tm.endpoint.Status status
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/removeProjectByNameRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/removeProjectByNameResponse")
    @RequestWrapper(localName = "removeProjectByName", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.RemoveProjectByName")
    @ResponseWrapper(localName = "removeProjectByNameResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.RemoveProjectByNameResponse")
    public void removeProjectByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/addProjectByUserRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/addProjectByUserResponse")
    @RequestWrapper(localName = "addProjectByUser", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.AddProjectByUser")
    @ResponseWrapper(localName = "addProjectByUserResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.AddProjectByUserResponse")
    public void addProjectByUser(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/finishProjectByNameRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/finishProjectByNameResponse")
    @RequestWrapper(localName = "finishProjectByName", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FinishProjectByName")
    @ResponseWrapper(localName = "finishProjectByNameResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FinishProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project finishProjectByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findProjectByNameRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findProjectByNameResponse")
    @RequestWrapper(localName = "findProjectByName", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindProjectByName")
    @ResponseWrapper(localName = "findProjectByNameResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project findProjectByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findProjectByIdRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/findProjectByIdResponse")
    @RequestWrapper(localName = "findProjectById", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindProjectById")
    @ResponseWrapper(localName = "findProjectByIdResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.FindProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project findProjectById(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/removeProjectByIndexRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/removeProjectByIndexResponse")
    @RequestWrapper(localName = "removeProjectByIndex", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.RemoveProjectByIndex")
    @ResponseWrapper(localName = "removeProjectByIndexResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.RemoveProjectByIndexResponse")
    public void removeProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/updateProjectByIndexResponse")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.UpdateProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project updateProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkova.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.tm.volkova.ru/ProjectEndpoint/updateProjectByIdResponse")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.tm.volkova.ru/", className = "ru.volkova.tm.endpoint.UpdateProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.volkova.tm.endpoint.Project updateProjectById(

        @WebParam(name = "session", targetNamespace = "")
        ru.volkova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );
}
