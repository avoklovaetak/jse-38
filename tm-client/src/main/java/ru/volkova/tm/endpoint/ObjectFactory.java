
package ru.volkova.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.volkova.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataBase64Load_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBase64Load");
    private final static QName _DataBase64LoadResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBase64LoadResponse");
    private final static QName _DataBase64Save_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBase64Save");
    private final static QName _DataBase64SaveResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBase64SaveResponse");
    private final static QName _DataBinaryLoad_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBinaryLoad");
    private final static QName _DataBinaryLoadResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBinaryLoadResponse");
    private final static QName _DataBinarySave_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBinarySave");
    private final static QName _DataBinarySaveResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataBinarySaveResponse");
    private final static QName _DataJsonLoad_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonLoad");
    private final static QName _DataJsonLoadFasterxml_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonLoadFasterxml");
    private final static QName _DataJsonLoadFasterxmlResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonLoadFasterxmlResponse");
    private final static QName _DataJsonLoadJaxb_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonLoadJaxb");
    private final static QName _DataJsonLoadJaxbResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonLoadJaxbResponse");
    private final static QName _DataJsonLoadResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonLoadResponse");
    private final static QName _DataJsonSave_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonSave");
    private final static QName _DataJsonSaveFasterxml_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonSaveFasterxml");
    private final static QName _DataJsonSaveFasterxmlResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonSaveFasterxmlResponse");
    private final static QName _DataJsonSaveJaxb_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonSaveJaxb");
    private final static QName _DataJsonSaveJaxbResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonSaveJaxbResponse");
    private final static QName _DataJsonSaveResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataJsonSaveResponse");
    private final static QName _DataXmlLoadFasterxml_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlLoadFasterxml");
    private final static QName _DataXmlLoadFasterxmlResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlLoadFasterxmlResponse");
    private final static QName _DataXmlLoadJaxb_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlLoadJaxb");
    private final static QName _DataXmlLoadJaxbResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlLoadJaxbResponse");
    private final static QName _DataXmlSaveFasterxml_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlSaveFasterxml");
    private final static QName _DataXmlSaveFasterxmlResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlSaveFasterxmlResponse");
    private final static QName _DataXmlSaveJaxb_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlSaveJaxb");
    private final static QName _DataXmlSaveJaxbResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataXmlSaveJaxbResponse");
    private final static QName _DataYamlLoadFasterxml_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataYamlLoadFasterxml");
    private final static QName _DataYamlLoadFasterxmlResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataYamlLoadFasterxmlResponse");
    private final static QName _DataYamlSaveFasterxml_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataYamlSaveFasterxml");
    private final static QName _DataYamlSaveFasterxmlResponse_QNAME = new QName("http://endpoint.tm.volkova.ru/", "dataYamlSaveFasterxmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.volkova.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataBase64Load }
     * 
     */
    public DataBase64Load createDataBase64Load() {
        return new DataBase64Load();
    }

    /**
     * Create an instance of {@link DataBase64LoadResponse }
     * 
     */
    public DataBase64LoadResponse createDataBase64LoadResponse() {
        return new DataBase64LoadResponse();
    }

    /**
     * Create an instance of {@link DataBase64Save }
     * 
     */
    public DataBase64Save createDataBase64Save() {
        return new DataBase64Save();
    }

    /**
     * Create an instance of {@link DataBase64SaveResponse }
     * 
     */
    public DataBase64SaveResponse createDataBase64SaveResponse() {
        return new DataBase64SaveResponse();
    }

    /**
     * Create an instance of {@link DataBinaryLoad }
     * 
     */
    public DataBinaryLoad createDataBinaryLoad() {
        return new DataBinaryLoad();
    }

    /**
     * Create an instance of {@link DataBinaryLoadResponse }
     * 
     */
    public DataBinaryLoadResponse createDataBinaryLoadResponse() {
        return new DataBinaryLoadResponse();
    }

    /**
     * Create an instance of {@link DataBinarySave }
     * 
     */
    public DataBinarySave createDataBinarySave() {
        return new DataBinarySave();
    }

    /**
     * Create an instance of {@link DataBinarySaveResponse }
     * 
     */
    public DataBinarySaveResponse createDataBinarySaveResponse() {
        return new DataBinarySaveResponse();
    }

    /**
     * Create an instance of {@link DataJsonLoad }
     * 
     */
    public DataJsonLoad createDataJsonLoad() {
        return new DataJsonLoad();
    }

    /**
     * Create an instance of {@link DataJsonLoadFasterxml }
     * 
     */
    public DataJsonLoadFasterxml createDataJsonLoadFasterxml() {
        return new DataJsonLoadFasterxml();
    }

    /**
     * Create an instance of {@link DataJsonLoadFasterxmlResponse }
     * 
     */
    public DataJsonLoadFasterxmlResponse createDataJsonLoadFasterxmlResponse() {
        return new DataJsonLoadFasterxmlResponse();
    }

    /**
     * Create an instance of {@link DataJsonLoadJaxb }
     * 
     */
    public DataJsonLoadJaxb createDataJsonLoadJaxb() {
        return new DataJsonLoadJaxb();
    }

    /**
     * Create an instance of {@link DataJsonLoadJaxbResponse }
     * 
     */
    public DataJsonLoadJaxbResponse createDataJsonLoadJaxbResponse() {
        return new DataJsonLoadJaxbResponse();
    }

    /**
     * Create an instance of {@link DataJsonLoadResponse }
     * 
     */
    public DataJsonLoadResponse createDataJsonLoadResponse() {
        return new DataJsonLoadResponse();
    }

    /**
     * Create an instance of {@link DataJsonSave }
     * 
     */
    public DataJsonSave createDataJsonSave() {
        return new DataJsonSave();
    }

    /**
     * Create an instance of {@link DataJsonSaveFasterxml }
     * 
     */
    public DataJsonSaveFasterxml createDataJsonSaveFasterxml() {
        return new DataJsonSaveFasterxml();
    }

    /**
     * Create an instance of {@link DataJsonSaveFasterxmlResponse }
     * 
     */
    public DataJsonSaveFasterxmlResponse createDataJsonSaveFasterxmlResponse() {
        return new DataJsonSaveFasterxmlResponse();
    }

    /**
     * Create an instance of {@link DataJsonSaveJaxb }
     * 
     */
    public DataJsonSaveJaxb createDataJsonSaveJaxb() {
        return new DataJsonSaveJaxb();
    }

    /**
     * Create an instance of {@link DataJsonSaveJaxbResponse }
     * 
     */
    public DataJsonSaveJaxbResponse createDataJsonSaveJaxbResponse() {
        return new DataJsonSaveJaxbResponse();
    }

    /**
     * Create an instance of {@link DataJsonSaveResponse }
     * 
     */
    public DataJsonSaveResponse createDataJsonSaveResponse() {
        return new DataJsonSaveResponse();
    }

    /**
     * Create an instance of {@link DataXmlLoadFasterxml }
     * 
     */
    public DataXmlLoadFasterxml createDataXmlLoadFasterxml() {
        return new DataXmlLoadFasterxml();
    }

    /**
     * Create an instance of {@link DataXmlLoadFasterxmlResponse }
     * 
     */
    public DataXmlLoadFasterxmlResponse createDataXmlLoadFasterxmlResponse() {
        return new DataXmlLoadFasterxmlResponse();
    }

    /**
     * Create an instance of {@link DataXmlLoadJaxb }
     * 
     */
    public DataXmlLoadJaxb createDataXmlLoadJaxb() {
        return new DataXmlLoadJaxb();
    }

    /**
     * Create an instance of {@link DataXmlLoadJaxbResponse }
     * 
     */
    public DataXmlLoadJaxbResponse createDataXmlLoadJaxbResponse() {
        return new DataXmlLoadJaxbResponse();
    }

    /**
     * Create an instance of {@link DataXmlSaveFasterxml }
     * 
     */
    public DataXmlSaveFasterxml createDataXmlSaveFasterxml() {
        return new DataXmlSaveFasterxml();
    }

    /**
     * Create an instance of {@link DataXmlSaveFasterxmlResponse }
     * 
     */
    public DataXmlSaveFasterxmlResponse createDataXmlSaveFasterxmlResponse() {
        return new DataXmlSaveFasterxmlResponse();
    }

    /**
     * Create an instance of {@link DataXmlSaveJaxb }
     * 
     */
    public DataXmlSaveJaxb createDataXmlSaveJaxb() {
        return new DataXmlSaveJaxb();
    }

    /**
     * Create an instance of {@link DataXmlSaveJaxbResponse }
     * 
     */
    public DataXmlSaveJaxbResponse createDataXmlSaveJaxbResponse() {
        return new DataXmlSaveJaxbResponse();
    }

    /**
     * Create an instance of {@link DataYamlLoadFasterxml }
     * 
     */
    public DataYamlLoadFasterxml createDataYamlLoadFasterxml() {
        return new DataYamlLoadFasterxml();
    }

    /**
     * Create an instance of {@link DataYamlLoadFasterxmlResponse }
     * 
     */
    public DataYamlLoadFasterxmlResponse createDataYamlLoadFasterxmlResponse() {
        return new DataYamlLoadFasterxmlResponse();
    }

    /**
     * Create an instance of {@link DataYamlSaveFasterxml }
     * 
     */
    public DataYamlSaveFasterxml createDataYamlSaveFasterxml() {
        return new DataYamlSaveFasterxml();
    }

    /**
     * Create an instance of {@link DataYamlSaveFasterxmlResponse }
     * 
     */
    public DataYamlSaveFasterxmlResponse createDataYamlSaveFasterxmlResponse() {
        return new DataYamlSaveFasterxmlResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64Load }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64Load }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBase64Load")
    public JAXBElement<DataBase64Load> createDataBase64Load(DataBase64Load value) {
        return new JAXBElement<DataBase64Load>(_DataBase64Load_QNAME, DataBase64Load.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64LoadResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64LoadResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBase64LoadResponse")
    public JAXBElement<DataBase64LoadResponse> createDataBase64LoadResponse(DataBase64LoadResponse value) {
        return new JAXBElement<DataBase64LoadResponse>(_DataBase64LoadResponse_QNAME, DataBase64LoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64Save }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64Save }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBase64Save")
    public JAXBElement<DataBase64Save> createDataBase64Save(DataBase64Save value) {
        return new JAXBElement<DataBase64Save>(_DataBase64Save_QNAME, DataBase64Save.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64SaveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBase64SaveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBase64SaveResponse")
    public JAXBElement<DataBase64SaveResponse> createDataBase64SaveResponse(DataBase64SaveResponse value) {
        return new JAXBElement<DataBase64SaveResponse>(_DataBase64SaveResponse_QNAME, DataBase64SaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinaryLoad }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinaryLoad }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBinaryLoad")
    public JAXBElement<DataBinaryLoad> createDataBinaryLoad(DataBinaryLoad value) {
        return new JAXBElement<DataBinaryLoad>(_DataBinaryLoad_QNAME, DataBinaryLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinaryLoadResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinaryLoadResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBinaryLoadResponse")
    public JAXBElement<DataBinaryLoadResponse> createDataBinaryLoadResponse(DataBinaryLoadResponse value) {
        return new JAXBElement<DataBinaryLoadResponse>(_DataBinaryLoadResponse_QNAME, DataBinaryLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinarySave }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinarySave }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBinarySave")
    public JAXBElement<DataBinarySave> createDataBinarySave(DataBinarySave value) {
        return new JAXBElement<DataBinarySave>(_DataBinarySave_QNAME, DataBinarySave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinarySaveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataBinarySaveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataBinarySaveResponse")
    public JAXBElement<DataBinarySaveResponse> createDataBinarySaveResponse(DataBinarySaveResponse value) {
        return new JAXBElement<DataBinarySaveResponse>(_DataBinarySaveResponse_QNAME, DataBinarySaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoad }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoad }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonLoad")
    public JAXBElement<DataJsonLoad> createDataJsonLoad(DataJsonLoad value) {
        return new JAXBElement<DataJsonLoad>(_DataJsonLoad_QNAME, DataJsonLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterxml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterxml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonLoadFasterxml")
    public JAXBElement<DataJsonLoadFasterxml> createDataJsonLoadFasterxml(DataJsonLoadFasterxml value) {
        return new JAXBElement<DataJsonLoadFasterxml>(_DataJsonLoadFasterxml_QNAME, DataJsonLoadFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterxmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadFasterxmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonLoadFasterxmlResponse")
    public JAXBElement<DataJsonLoadFasterxmlResponse> createDataJsonLoadFasterxmlResponse(DataJsonLoadFasterxmlResponse value) {
        return new JAXBElement<DataJsonLoadFasterxmlResponse>(_DataJsonLoadFasterxmlResponse_QNAME, DataJsonLoadFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonLoadJaxb")
    public JAXBElement<DataJsonLoadJaxb> createDataJsonLoadJaxb(DataJsonLoadJaxb value) {
        return new JAXBElement<DataJsonLoadJaxb>(_DataJsonLoadJaxb_QNAME, DataJsonLoadJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonLoadJaxbResponse")
    public JAXBElement<DataJsonLoadJaxbResponse> createDataJsonLoadJaxbResponse(DataJsonLoadJaxbResponse value) {
        return new JAXBElement<DataJsonLoadJaxbResponse>(_DataJsonLoadJaxbResponse_QNAME, DataJsonLoadJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonLoadResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonLoadResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonLoadResponse")
    public JAXBElement<DataJsonLoadResponse> createDataJsonLoadResponse(DataJsonLoadResponse value) {
        return new JAXBElement<DataJsonLoadResponse>(_DataJsonLoadResponse_QNAME, DataJsonLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSave }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSave }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonSave")
    public JAXBElement<DataJsonSave> createDataJsonSave(DataJsonSave value) {
        return new JAXBElement<DataJsonSave>(_DataJsonSave_QNAME, DataJsonSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterxml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterxml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonSaveFasterxml")
    public JAXBElement<DataJsonSaveFasterxml> createDataJsonSaveFasterxml(DataJsonSaveFasterxml value) {
        return new JAXBElement<DataJsonSaveFasterxml>(_DataJsonSaveFasterxml_QNAME, DataJsonSaveFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterxmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveFasterxmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonSaveFasterxmlResponse")
    public JAXBElement<DataJsonSaveFasterxmlResponse> createDataJsonSaveFasterxmlResponse(DataJsonSaveFasterxmlResponse value) {
        return new JAXBElement<DataJsonSaveFasterxmlResponse>(_DataJsonSaveFasterxmlResponse_QNAME, DataJsonSaveFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonSaveJaxb")
    public JAXBElement<DataJsonSaveJaxb> createDataJsonSaveJaxb(DataJsonSaveJaxb value) {
        return new JAXBElement<DataJsonSaveJaxb>(_DataJsonSaveJaxb_QNAME, DataJsonSaveJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonSaveJaxbResponse")
    public JAXBElement<DataJsonSaveJaxbResponse> createDataJsonSaveJaxbResponse(DataJsonSaveJaxbResponse value) {
        return new JAXBElement<DataJsonSaveJaxbResponse>(_DataJsonSaveJaxbResponse_QNAME, DataJsonSaveJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonSaveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataJsonSaveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataJsonSaveResponse")
    public JAXBElement<DataJsonSaveResponse> createDataJsonSaveResponse(DataJsonSaveResponse value) {
        return new JAXBElement<DataJsonSaveResponse>(_DataJsonSaveResponse_QNAME, DataJsonSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterxml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterxml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlLoadFasterxml")
    public JAXBElement<DataXmlLoadFasterxml> createDataXmlLoadFasterxml(DataXmlLoadFasterxml value) {
        return new JAXBElement<DataXmlLoadFasterxml>(_DataXmlLoadFasterxml_QNAME, DataXmlLoadFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterxmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadFasterxmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlLoadFasterxmlResponse")
    public JAXBElement<DataXmlLoadFasterxmlResponse> createDataXmlLoadFasterxmlResponse(DataXmlLoadFasterxmlResponse value) {
        return new JAXBElement<DataXmlLoadFasterxmlResponse>(_DataXmlLoadFasterxmlResponse_QNAME, DataXmlLoadFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlLoadJaxb")
    public JAXBElement<DataXmlLoadJaxb> createDataXmlLoadJaxb(DataXmlLoadJaxb value) {
        return new JAXBElement<DataXmlLoadJaxb>(_DataXmlLoadJaxb_QNAME, DataXmlLoadJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlLoadJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlLoadJaxbResponse")
    public JAXBElement<DataXmlLoadJaxbResponse> createDataXmlLoadJaxbResponse(DataXmlLoadJaxbResponse value) {
        return new JAXBElement<DataXmlLoadJaxbResponse>(_DataXmlLoadJaxbResponse_QNAME, DataXmlLoadJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterxml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterxml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlSaveFasterxml")
    public JAXBElement<DataXmlSaveFasterxml> createDataXmlSaveFasterxml(DataXmlSaveFasterxml value) {
        return new JAXBElement<DataXmlSaveFasterxml>(_DataXmlSaveFasterxml_QNAME, DataXmlSaveFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterxmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveFasterxmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlSaveFasterxmlResponse")
    public JAXBElement<DataXmlSaveFasterxmlResponse> createDataXmlSaveFasterxmlResponse(DataXmlSaveFasterxmlResponse value) {
        return new JAXBElement<DataXmlSaveFasterxmlResponse>(_DataXmlSaveFasterxmlResponse_QNAME, DataXmlSaveFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlSaveJaxb")
    public JAXBElement<DataXmlSaveJaxb> createDataXmlSaveJaxb(DataXmlSaveJaxb value) {
        return new JAXBElement<DataXmlSaveJaxb>(_DataXmlSaveJaxb_QNAME, DataXmlSaveJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataXmlSaveJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataXmlSaveJaxbResponse")
    public JAXBElement<DataXmlSaveJaxbResponse> createDataXmlSaveJaxbResponse(DataXmlSaveJaxbResponse value) {
        return new JAXBElement<DataXmlSaveJaxbResponse>(_DataXmlSaveJaxbResponse_QNAME, DataXmlSaveJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterxml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterxml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataYamlLoadFasterxml")
    public JAXBElement<DataYamlLoadFasterxml> createDataYamlLoadFasterxml(DataYamlLoadFasterxml value) {
        return new JAXBElement<DataYamlLoadFasterxml>(_DataYamlLoadFasterxml_QNAME, DataYamlLoadFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterxmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlLoadFasterxmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataYamlLoadFasterxmlResponse")
    public JAXBElement<DataYamlLoadFasterxmlResponse> createDataYamlLoadFasterxmlResponse(DataYamlLoadFasterxmlResponse value) {
        return new JAXBElement<DataYamlLoadFasterxmlResponse>(_DataYamlLoadFasterxmlResponse_QNAME, DataYamlLoadFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterxml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterxml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataYamlSaveFasterxml")
    public JAXBElement<DataYamlSaveFasterxml> createDataYamlSaveFasterxml(DataYamlSaveFasterxml value) {
        return new JAXBElement<DataYamlSaveFasterxml>(_DataYamlSaveFasterxml_QNAME, DataYamlSaveFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterxmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataYamlSaveFasterxmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkova.ru/", name = "dataYamlSaveFasterxmlResponse")
    public JAXBElement<DataYamlSaveFasterxmlResponse> createDataYamlSaveFasterxmlResponse(DataYamlSaveFasterxmlResponse value) {
        return new JAXBElement<DataYamlSaveFasterxmlResponse>(_DataYamlSaveFasterxmlResponse_QNAME, DataYamlSaveFasterxmlResponse.class, null, value);
    }

}
