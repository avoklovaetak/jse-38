package ru.volkova.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.List;

public class AdminUserEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addUserTest() {
        User user = new User();
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addAllUsersTest(){
        User user1 = new User();
        user1 = endpointLocator.getAdminUserEndpoint().addUser(session, user1);
        User user2 = new User();
        user2 = endpointLocator.getAdminUserEndpoint().addUser(session, user2);
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        endpointLocator.getAdminUserEndpoint().addAllUsers(session, users);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user1.id));
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user2.id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createUserByLogPassTest() {
        final String login = "test1";
        final String password = "test";
        final User user = endpointLocator.getAdminUserEndpoint()
                .createUserByLogPass(session, login, password);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint().findUserById(session, user.id));
        final Session session1 = endpointLocator.getSessionEndpoint()
                .openSession(login, password);
        Assert.assertNotNull(session1);
        endpointLocator.getSessionEndpoint().closeSession(session1);
        endpointLocator.getAdminUserEndpoint().removeUser(session, user);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createUserWithEmailTest() {
        final String login = "log";
        final String password = "test";
        final String email = "test@test.ru";
        final User user = endpointLocator.getAdminUserEndpoint()
                .createUserWithEmail(session, login, password, email);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint().findUserById(session, user.id));
        final Session session1 = endpointLocator.getSessionEndpoint()
                .openSession(login, password);
        Assert.assertNotNull(session1);
        endpointLocator.getSessionEndpoint().closeSession(session1);
        endpointLocator.getAdminUserEndpoint().removeUser(session, user);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createUserWithRoleTest() {
        final String login = "test3";
        final String password = "test";
        final Role role = Role.USER;
        final User user = endpointLocator.getAdminUserEndpoint()
                .createUserWithRole(session, login, password, role);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint().findUserById(session, user.id));
        final Session session1 = endpointLocator.getSessionEndpoint()
                .openSession(login, password);
        Assert.assertNotNull(session1);
        endpointLocator.getSessionEndpoint().closeSession(session1);
        endpointLocator.getAdminUserEndpoint().removeUser(session, user);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findUserByIdTest() {
        User user = new User();
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        User user = new User();
        user.setLogin("login");
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByEmailTest() {
        User user = new User();
        user.setEmail("testtest@test.ru");
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByEmail(session, user.email);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByEmail(session, user.email);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByIdTest() {
        User user = new User();
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockById(session, user.id);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserById(session, user.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByLoginTest() {
        User user = new User();
        user.setLogin("qwerty");
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByLogin(session, user.login);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, user.login);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unlockUserByEmailTest() {
        User user = new User();
        user.setEmail("testtest@test.ru");
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByEmail(session, user.email);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().unlockUserByEmail(session, user.email);
        Assert.assertTrue(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByEmail(session, user.email);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unlockUserByIdTest() {
        User user = new User();
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockById(session, user.id);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().unlockUserById(session, user.id);
        Assert.assertTrue(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserById(session, user.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unlockUserByLoginTest() {
        User user = new User();
        user.setLogin("qwerty");
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByLogin(session, user.login);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login).locked);
        endpointLocator.getAdminUserEndpoint().unlockUserByLogin(session, user.login);
        Assert.assertTrue(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, user.login);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        User user = new User();
        user.setLogin("login1");
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login));
        endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, user.login);
        Assert.assertNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeUserByIdTest() {
        User user = new User();
        user = endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id));
        endpointLocator.getAdminUserEndpoint().removeUserById(session, user.id);
        Assert.assertNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.id));
    }

}
