package ru.volkova.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

public class TaskEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addTaskByUserTest() {
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, "task", "test");
        Task task = endpointLocator.getTaskEndpoint()
                .findTaskByName(session, "task");
        Assert.assertNotNull(task);
        Assert.assertEquals("task", task.name);
        Assert.assertEquals("test", task.description);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByIdTest() {
        final Status status = Status.COMPLETE;
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, "project", "test");
        Task task = endpointLocator.getTaskEndpoint()
                .changeTaskStatusByName(session, "project", status);
        Assert.assertEquals(status, task.status);
        endpointLocator.getTaskEndpoint().removeTaskById(session, task.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByNameTest() {
        final Status status = Status.COMPLETE;
        final String name = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, "test");
        Task task = endpointLocator.getTaskEndpoint()
                .changeTaskStatusByName(session, name, status);
        Assert.assertEquals(status, task.status);
        endpointLocator.getTaskEndpoint().removeTaskById(session, task.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Assert.assertNotNull(endpointLocator.getTaskEndpoint()
                .findTaskByName(session, name));
        endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Assert.assertNotNull(endpointLocator.getTaskEndpoint()
                .findTaskByName(session, name));
        endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void finishProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Assert.assertEquals(Status.COMPLETE, endpointLocator.getTaskEndpoint()
                .finishTaskByName(session, name).status);
        endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void finishProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Assert.assertEquals(Status.COMPLETE, endpointLocator.getTaskEndpoint()
                .finishTaskByName(session, name).status);
        endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void startProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Assert.assertEquals(Status.IN_PROGRESS, endpointLocator.getTaskEndpoint()
                .startTaskByName(session, name).status);
        endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void startProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Assert.assertEquals(Status.IN_PROGRESS, endpointLocator.getTaskEndpoint()
                .startTaskByName(session, name).status);
        endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void removeProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Task task = endpointLocator.getTaskEndpoint().finishTaskByName(session,name);
        endpointLocator.getTaskEndpoint().removeTaskById(session, task.id);
        endpointLocator.getTaskEndpoint().findTaskById(session, task.id);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void removeProjectByNameTest(){
        final String name = "test-project1";
        final String description = "test-project1";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
        endpointLocator.getTaskEndpoint().findTaskByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateProjectByIdTest() {
        final String name = "test-project";
        final String upd_name = "test";
        final String description = "test-project";
        final String upd_description = "test";
        endpointLocator.getTaskEndpoint()
                .addTaskByUser(session, name, description);
        Task task = endpointLocator.getTaskEndpoint().finishTaskByName(session,name);
        task = endpointLocator.getTaskEndpoint()
                .updateTaskById(session, task.id, upd_name, upd_description);
        Assert.assertEquals(upd_name, task.name);
        Assert.assertEquals(upd_description, task.description);
        endpointLocator.getTaskEndpoint().removeTaskById(session, task.id);
    }

}
