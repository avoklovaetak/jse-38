package ru.volkova.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.repository.UserRepository;

public class UserServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IUserRepository userRepository = new UserRepository(connectionService);

    private final IUserService userService = new UserService(userRepository, propertyService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(userService.add(user));
    }

}
