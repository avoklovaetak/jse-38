package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final User user = new User();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Task task = new Task();
        Assert.assertNotNull(taskService.add(task));
    }

    @Test
    @Category(UnitCategory.class)
    public void addWithParametersTest() {
        taskService.add("1", "Project 1", "it is project");
        final Task task = taskService.findOneByName(user.getId(), "Project 1");
        Assert.assertNotNull(task);
        Assert.assertEquals("1", task.getUserId());
        Assert.assertEquals("Project 1", task.getName());
        Assert.assertEquals("it is project", task.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Task> taskList = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        task1.setUserId(user.getId());
        task2.setUserId(user.getId());
        taskList.add(task1);
        taskList.add(task2);
        taskRepository.addAll(taskList);
        Assert.assertEquals(
                taskService.findById(task1.getUserId(),task1.getId()),
                task1);
        Assert.assertEquals(
                taskService.findById(task2.getUserId(),task2.getId()),
                task2);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.add(task);
        Assert.assertNotNull(taskService.findAll(task.getUserId()));
        taskService.remove(task);
        Assert.assertTrue(taskService.findAll(task.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.add(task);
        Assert.assertNotNull(taskService.findAll(task.getUserId()));
        taskService.clear(user.getId());
        Assert.assertTrue(taskService.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIdTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.add(task);
        Assert.assertEquals(
                taskService.findById(task.getUserId(), task.getId()),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIndexTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.add(task);
        Assert.assertEquals(
                taskService.findOneByIndex(task.getUserId(), 0),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByNameTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        task.setName("DEMO");
        taskService.add(task);
        Assert.assertEquals(
                taskService.findOneByName(task.getUserId(), task.getName()),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.add(task);
        Assert.assertNotNull(taskService.findById(task.getUserId(), task.getId()));
        taskService.removeById(task.getUserId(), task.getId());
        Assert.assertTrue(taskService.findAll(task.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.add(task);
        Assert.assertEquals(
                taskService.findOneByIndex(task.getUserId(), 0),
                task);
        taskService.removeOneByIndex(task.getUserId(), 0);
        Assert.assertTrue(taskService.findAll(task.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        task.setName("DEMO");
        taskService.add(task);
        Assert.assertEquals(
                taskService.findOneByName(task.getUserId(), task.getName()),
                task);
        taskService.removeOneByName(task.getUserId(), task.getName());
        Assert.assertTrue(taskService.findAll(task.getUserId()).isEmpty());
    }

}
