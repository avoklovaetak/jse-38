package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.ConnectionService;
import ru.volkova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connectionService);

    @NotNull
    private final User user = new User();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Project project = new Project();
        Assert.assertNotNull(projectRepository.add(project));
    }

    @Test
    @Category(UnitCategory.class)
    public void addWithParametersTest() {
        projectRepository.add("1", "Project 1", "it is project");
        final Project project = projectRepository.findById("1", "Project 1");
        Assert.assertNotNull(project);
        Assert.assertEquals("1", project.getUserId());
        Assert.assertEquals("Project 1", project.getName());
        Assert.assertEquals("it is project", project.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Project> projectList = new ArrayList<Project>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        project1.setUserId(user.getId());
        project2.setUserId(user.getId());
        projectList.add(project1);
        projectList.add(project2);
        projectRepository.addAll(projectList);
        Assert.assertEquals(
                projectRepository.findById(project1.getUserId(),project1.getId()
                ), project1);
        Assert.assertEquals(
                projectRepository.findById(project2.getUserId(),project2.getId()),
                project2);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findAll(project.getUserId()));
        projectRepository.remove(project);
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findAll(project.getUserId()));
        projectRepository.clear(user.getId());
        Assert.assertTrue(projectRepository.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertEquals(
                projectRepository.findById(project.getUserId(), project.getId()),
                project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIndexTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertEquals(projectRepository.findOneByIndex(project.getUserId(), 0),
                project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByNameTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("DEMO");
        projectRepository.add(project);
        Assert.assertEquals(
                projectRepository.findOneByName(project.getUserId(), project.getName()),
                project);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findById(project.getUserId(), project.getId()));
        projectRepository.removeById(project.getUserId(), project.getId());
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectRepository.add(project);
        Assert.assertEquals(projectRepository.findOneByIndex(project.getUserId(), 0),
                project);
        projectRepository.removeOneByIndex(project.getUserId(), 0);
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("DEMO");
        projectRepository.add(project);
        Assert.assertEquals(
                projectRepository.findOneByName(project.getUserId(), project.getName()),
                project);
        projectRepository.removeOneByName(project.getUserId(), project.getName());
        Assert.assertTrue(projectRepository.findAll(project.getUserId()).isEmpty());
    }

}
