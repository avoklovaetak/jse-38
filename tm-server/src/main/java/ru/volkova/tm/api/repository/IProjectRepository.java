package ru.volkova.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Project;

import java.sql.ResultSet;
import java.util.Optional;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @Nullable
    Project insert(@Nullable final Project project);

    void add(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

}
