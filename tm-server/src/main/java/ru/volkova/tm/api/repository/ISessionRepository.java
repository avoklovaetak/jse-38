package ru.volkova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

}
