package ru.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task addTask(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") Task entity
    );

    @WebMethod
    void removeTask(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") Task entity
    );

    @WebMethod
    void addAllTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "entities", partName = "entities") List<Task> entities);

    @WebMethod
    void clearTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @Nullable
    @WebMethod
    List<Task> findAllTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @Nullable
    @WebMethod
    Task findTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void removeTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Task changeTaskStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "id") Status status
    );

    @Nullable
    @WebMethod
    Task changeTaskStatusByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @Nullable
    @WebMethod
    Task changeTaskStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @Nullable
    @WebMethod
    Task findTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Task findTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    Task finishTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Task finishTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Task finishTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index);

    @WebMethod
    void removeTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    Task startTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Task startTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Task startTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    Task updateTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @Nullable
    @WebMethod
    Task updateTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    void addTaskByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

}
